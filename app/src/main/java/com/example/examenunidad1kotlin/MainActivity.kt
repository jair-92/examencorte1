package com.example.examenunidad1kotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.Toast
import androidx.appcompat.app.AlertDialog
import android.content.Intent


class MainActivity : AppCompatActivity() {
    private lateinit var txtNumCuenta: EditText
    private lateinit var txtNombre: EditText
    private lateinit var txtBanco: EditText
    private lateinit var txtSaldo: EditText
    private lateinit var btnEnviar: Button
    private lateinit var btnSalir: Button

    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_main)
        iniciarComponentes()
        //Eventos enviar y salir
        btnEnviar.setOnClickListener { enviar() }
        btnSalir.setOnClickListener { salir() }
    }

    private fun iniciarComponentes() {
        //Componentes xml
        txtNumCuenta = findViewById(R.id.txtNumCuenta)
        txtNombre = findViewById(R.id.txtNombre)
        txtBanco = findViewById(R.id.txtBanco)
        txtSaldo = findViewById(R.id.txtSaldo)
        btnEnviar = findViewById(R.id.btnEnviar)
        btnSalir = findViewById(R.id.btnSalir)
    }

    private fun enviar() {
        if(txtNumCuenta.text.toString().isEmpty() || txtNombre.text.toString().isEmpty() ||
            txtBanco.text.toString().isEmpty() || txtSaldo.text.toString().isEmpty()){
            Toast.makeText(
                this.applicationContext, "Faltan datos",
                Toast.LENGTH_SHORT).show()
        } else {
            //Enviando información
            var bundle = Bundle()
            bundle.putString("banco", txtBanco.text.toString())
            bundle.putString("nombre", txtNombre.text.toString())
            bundle.putString("saldo",  txtSaldo.text.toString())

            //Intent llamando
            var intent = Intent(this@MainActivity, CuentaBancoActivity::class.java)
            intent.putExtras(bundle)

            //Actividad iniciando
            startActivity(intent)
        }
    }

    private fun salir(){
        val confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("Cuenta de Banco")
        confirmar.setMessage("¿Desea salir de la app?")
        confirmar.setPositiveButton("Confirmar") { dialog, which -> finish() }
        confirmar.setNegativeButton("Cancelar") { dialog, which ->  }
        confirmar.show()
    }



}