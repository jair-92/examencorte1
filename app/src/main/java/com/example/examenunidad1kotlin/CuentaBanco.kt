package com.example.examenunidad1kotlin

class CuentaBanco {
    // Declaración de varibles
    private var numCuenta = 0
    private var nombre: String? = null
    private var banco: String? = null
    private var saldo = 0f

    //Constructor
    fun CuentaBanco(numCuenta: Int, nombre: String?, banco:String?, saldo: Float){
        this.numCuenta = numCuenta
        this.nombre = nombre
        this.banco = banco
        this.saldo = saldo

    }

    fun setSaldo(saldo: Float) {
        this.saldo = saldo
    }

    fun getSaldo():Float {
        return saldo
    }


    //funciones calcular
    fun depositar(saldoD: Float){
        this.saldo = this.saldo + saldoD

    }

    fun retirar(saldoR: Float): Boolean {
        //saldo insuficiente
        if(saldoR > this.saldo){
            return false
        }
        //saldo suficiente
        this.saldo = this.saldo - saldoR
        return true

    }




}