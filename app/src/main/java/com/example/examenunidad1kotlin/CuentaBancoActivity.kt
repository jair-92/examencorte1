package com.example.examenunidad1kotlin

import androidx.appcompat.app.AppCompatActivity
import android.os.Bundle
import android.widget.Button
import android.widget.EditText
import android.widget.TextView
import android.widget.Toast
import androidx.appcompat.app.AlertDialog

class CuentaBancoActivity : AppCompatActivity() {
    //Declarar
    private lateinit var lblNombreBanco: TextView
    private lateinit var lblNombreCliente: TextView
    private lateinit var lblSaldo: TextView
    private lateinit var txtCantidad: EditText
    private lateinit var btnDeposito: Button
    private lateinit var btnRetiro: Button
    private lateinit var btnRegresar: Button
    private var cuentaBanco = CuentaBanco()


    private var saldoInicial: String? = null
    private var nombreBanco: String? = null
    private var nombreCliente: String? = null


    override fun onCreate(savedInstanceState: Bundle?) {
        super.onCreate(savedInstanceState)
        setContentView(R.layout.activity_cuenta_banco)
        iniciarComponentes()

        // Obtener los datos del MainActivity
        var datos = intent.extras
        nombreBanco = datos!!.getString("banco")
        nombreCliente = datos!!.getString("nombre")
        saldoInicial = datos!!.getString("saldo")

        // Asignar el texto a las etiquetas
        lblNombreBanco.text = nombreBanco
        lblNombreCliente.text = nombreCliente
        lblSaldo.text = saldoInicial

        // Evento click
        btnDeposito.setOnClickListener { depositar() }
        btnRetiro.setOnClickListener { retirar() }
        btnRegresar.setOnClickListener { regresar() }


    }

    private fun iniciarComponentes() {
        lblNombreBanco = findViewById(R.id.lblNombreBanco)
        lblNombreCliente = findViewById(R.id.lblNombreCliente)
        lblSaldo = findViewById(R.id.lblSaldo)

        txtCantidad = findViewById(R.id.txtCantidad)

        btnDeposito = findViewById(R.id.btnDeposito)
        btnRetiro = findViewById(R.id.btnRetiro)
        btnRegresar = findViewById(R.id.btnRegresar)

        cuentaBanco = CuentaBanco()
    }

    private fun depositar() {
        var saldoD = 0f
        cuentaBanco.setSaldo(saldoInicial.toString().toFloat())

        if(txtCantidad.text.toString().isEmpty()){
            Toast.makeText(
                this.applicationContext, "Ingrese una cantidad",
                Toast.LENGTH_SHORT).show()
        }else {
            saldoD = txtCantidad.text.toString().toFloat()
            cuentaBanco.depositar(saldoD)
            lblSaldo.setText(cuentaBanco.getSaldo().toString())
            saldoInicial = cuentaBanco.getSaldo().toString()
        }

    }

    private fun retirar() {
        if(txtCantidad.text.toString().isEmpty()){
            Toast.makeText(
                this.applicationContext, "Ingrese una cantidad",
                Toast.LENGTH_SHORT).show()
        }else {
            var saldoR = txtCantidad.text.toString().toFloat()

            if(!cuentaBanco.retirar(saldoR)){
                Toast.makeText(this.applicationContext, "Fondos insuficientes",
                    Toast.LENGTH_SHORT).show()
            } else {
                lblSaldo.setText(cuentaBanco.getSaldo().toString())
                saldoInicial = cuentaBanco.getSaldo().toString()
            }
        }

    }

    private fun regresar() {
        val confirmar = AlertDialog.Builder(this)
        confirmar.setTitle("Cuenta de Banco")
        confirmar.setMessage("¿Desea regresar al inicio?")
        confirmar.setPositiveButton("Confirmar") { dialog, which -> finish() }
        confirmar.setNegativeButton("Cancelar") { dialog, which ->  }
        confirmar.show()
    }




}